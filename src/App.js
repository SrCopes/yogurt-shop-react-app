import React, { useState } from "react";
import "./Styles/Styles.css";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import UserPage from "./components/UserPage/UserPage";
import LoginPage from "./components/LoginPage/LoginPage";
import MainPage from "./components/MainPage/MainPage";
import NavbarComponent from "./components/NavbarComponent/NavbarComponent";
import InvoicePage from "./components/InvoicePage";
import SavedToken from "./components/savedToken";
import Footer from "./components/Footer";

export const Context = React.createContext();

export default function App() {
  const [shoppingItems, setShoppingItems] = useState([]);
  console.log(Context);
  const { token, setToken } = SavedToken();
  if (!token) {
    return <LoginPage setToken={setToken} />;
  }

  return (
    <div>
      <Context.Provider
        value={{
          shoppingItems: shoppingItems,
          setShoppingItems: setShoppingItems,
        }}
      >
        {console.log(shoppingItems)}
        <BrowserRouter>
          <>
            <NavbarComponent shoppingItem={shoppingItems} />
            <Switch>
              <Route exact path="/" component={MainPage}></Route>
              <Route path="/userPage" component={UserPage}></Route>
              <Route exact path="/shopKart" component={InvoicePage}></Route>
            </Switch>
          </>
        </BrowserRouter>
      </Context.Provider>
    </div>
  );
}
