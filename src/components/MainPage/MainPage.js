import React, { useState, useEffect, useContext } from "react";
import "../../Styles/Styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import ProductDisplay from "../ProductDisplay/ProductDisplay";
import axios from "axios";
import { Context } from "../../App";

export default function MainPage() {
  const [yogurt, setYogurt] = useState([]);
  const [searchYogurt, setSearchYogurt] = useState({
    products: "",
    img: "",
    name: "",
  });

  const [yogurtName, setYogurtName] = useState("");

  const [yogurtChosen, setYogurtChosen] = useState(false);

  const { shoppingItems, setShoppingItems } = useContext(Context);

  function handleSelect(name) {
    setShoppingItems((prevValue) => [...prevValue, name]);
  }

  const getYogurt = async () => {
    const res = await fetch(
      `https://mocki.io/v1/78fa5602-1543-4e6d-8a99-a842ac3f1c0f`
    );
    const data = await res.json();

    setYogurt(data);
  };

  useEffect(() => {
    getYogurt();
  }, []);

  /* Search */

  const searchYogurtAPI = () => {
    axios
      .get(
        `https://mocki.io/v1/78fa5602-1543-4e6d-8a99-a842ac3f1c0f${yogurtName}` //not working, only will work if the api have the endpoint for name
      )
      .then((res) => {
        setSearchYogurt({
          name: yogurtName,
          img: res.data.img,
          products: res.data.products,
        });
        setYogurtChosen(true);
      });
  };

  return (
    <div className="main-container">
      <Container fluid>
        <Row>
          <Col sm="12" md="8" lg="6">
            <div className="top-main">
              <h1>soWow</h1>
              <h4>The best yogurt you've ever tasted!</h4>
              <h6>Don't forget to try our new flavors!</h6>

              <div className="search-and-btn">
                <input
                  className="searchBar"
                  type="text"
                  onChange={(event) => {
                    setYogurtName(event.target.value);
                  }}
                  value={yogurtName.toLowerCase()}
                />
                {yogurtName && (
                  <Button
                    as="input"
                    type="button"
                    value="search..."
                    variant="outlined"
                    onClick={searchYogurtAPI}
                    placeholder="search..."
                    className="searchBtn"
                  />
                )}
              </div>
              <div className="main-bottom">
                {!yogurtChosen ? (
                  <p>search to find your favorite products</p>
                ) : (
                  <>
                    <div className="yoguInfo">
                      <h1>{searchYogurt.products}</h1>
                      <img src={searchYogurt.img} alt={searchYogurt.products} />
                    </div>
                  </>
                )}
              </div>
            </div>
          </Col>

          <Col sm="4" md="4" lg="3">
            <div>
              {yogurt.length > 0 && //acrescentei isto porque dizia no stackoverflow para tentar que o .map funcionasse
                yogurt.map((elem, index) => (
                  <ProductDisplay
                    key={index}
                    info={elem}
                    handleSelect={handleSelect}
                  />
                ))}
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
