import React from "react";
import "../../Styles/Styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "react-bootstrap/Navbar";
import { NavLink } from "react-router-dom";
import Nav from "react-bootstrap/Nav";

export default function NavbarComponent(props) {
  return (
    <Navbar className="nav-container">
      {/* <Container className="navbar"> */}
      <Navbar.Brand className="navbar-title" to={"/"}>
        soWow
      </Navbar.Brand>

      <Nav>
        <NavLink className="navbar-text" to={"/"}>
          Home
        </NavLink>

        <NavLink className="navbar-text" to={"/userPage"}>
          My Profile
        </NavLink>

        <NavLink className="navbar-text" to={"/shopKart"}>
          Shop Kart
        </NavLink>
      </Nav>
      {/* </Container> */}
    </Navbar>
  );
}
