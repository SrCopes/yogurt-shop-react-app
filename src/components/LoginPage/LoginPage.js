import React, { useState } from "react";
import "../../Styles/Styles.css";
import PropTypes from "prop-types";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

async function loginUser(credentials) {
  return fetch("http://localhost:8080/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}

export default function LoginPage({ setToken }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await loginUser({
      username,
      password,
    });
    setToken(token);
  };
  return (
    <div className="login-background">
      <Container fluid>
        <Row>
          <Col xs="12" md="12" lg="12" className="login-container">
            <h2>Login</h2>
            <form onSubmit={handleSubmit} className="login-form">
              <label>
                <input
                  className="input-form"
                  type="text"
                  onChange={(e) => setUserName(e.target.value)}
                  placeholder="username"
                />
              </label>
              <label>
                <input
                  className="input-form"
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="password"
                />
              </label>
              <div>
                <button className="formBtn" type="submit">
                  Login
                </button>
              </div>
            </form>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

LoginPage.propTypes = {
  setToken: PropTypes.func.isRequired,
};
