import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function UserPage() {
  const [editNote, setEditNote] = useState(false);
  const [userSetting, setUserSetting] = useState({
    username: "",
    password: "",
  });
  const [newUserSettings, setNewUserSettings] = useState({
    username: "",
    password: "",
  });

  const primitiveUserName = "Filipe";
  const primitivePassword = "test123";

  const handleEdit = () => {
    setEditNote(true);
  };

  const handleChangeUserName = (event) => {
    setUserSetting((prevValue) => ({
      ...prevValue,
      username: event.target.value,
    }));
  };
  const handleChangePassword = (event) => {
    setUserSetting((prevValue) => ({
      ...prevValue,
      password: event.target.value,
    }));
  };
  const handleSave = () => {
    setNewUserSettings({
      username: userSetting.username,
      password: userSetting.password,
    });
    setEditNote(false);
  };
  return (
    <div className="user-container">
      <Container>
        <Col>
          <form>
            <label>
              <Row sm="12" md="8" lg="6">
                <h2>Your Username</h2>
                <h4>
                  {newUserSettings.username !== ""
                    ? newUserSettings.username
                    : primitiveUserName}
                </h4>
                <div>
                  <button className="userBtn" onClick={handleEdit}>
                    Edit
                  </button>
                </div>

                {editNote ? (
                  <div>
                    <input
                      className="input-user-save"
                      onChange={handleChangeUserName}
                      value={userSetting.username}
                      type="text"
                    />
                    <div>
                      <button className="userBtn-save" onClick={handleSave}>
                        save
                      </button>
                    </div>
                  </div>
                ) : null}
              </Row>
            </label>
            <label>
              <Row sm="12" md="8" lg="6">
                <h2>New Password</h2>
                <h4>
                  {newUserSettings.password !== ""
                    ? newUserSettings.password
                    : primitivePassword}
                </h4>
                <div>
                  <button className="userBtn" onClick={handleEdit}>
                    Edit
                  </button>
                </div>
                {editNote ? (
                  <div>
                    <input
                      className="input-user-save"
                      onChange={handleChangePassword}
                      value={userSetting.password}
                      type="password"
                    />
                    <div>
                      <button className="userBtn-save" onClick={handleSave}>
                        save
                      </button>
                    </div>
                  </div>
                ) : null}
              </Row>
            </label>
          </form>
        </Col>
      </Container>
    </div>
  );
}
