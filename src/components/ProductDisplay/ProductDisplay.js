import React, { useContext, useState } from "react";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import { CardActions } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";

export default function ProductDisplay(props) {
  function handleClick() {
    props.handleSelect(props.info);
  }

  return (
    <div className="card-container">
      <Card sx={{ width: 150, height: 250, marginBottom: 2 }}>
        <CardActionArea>
          <CardMedia
            component="img"
            height="120"
            image={props.info.img}
            alt={props.info.name}
          />
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              style={{ color: "rgb(125, 197, 202)", fontSize: 15 }}
            >
              {props.info.products}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              style={{ color: "rgb(134, 145, 146)", fontSize: 12 }}
            >
              {props.info.total}€
            </Typography>
          </CardContent>
        </CardActionArea>

        <CardActions disableSpacing>
          <IconButton
            aria-label="add to shopping list"
            style={{ height: 4, marginTop: -4 }}
            onClick={handleClick}
          >
            <ShoppingCartIcon />
          </IconButton>
        </CardActions>
      </Card>
    </div>
  );
}
