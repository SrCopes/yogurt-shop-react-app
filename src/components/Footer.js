import React from "react";
import { Link } from "react-router-dom";
import "./css/footer.css";

export default function Footer() {
  return (
    <section className="footer">
      <div className="box-container">
        <div className="box">
          <h3>about us</h3>
          <p>
            We made our yogurts with care thinking always in the custommer
            first.
          </p>
        </div>
        <div className="box">
          <h3>countrys avaiable</h3>
          <strong>Portugal</strong>
          <strong>England</strong>
          <strong>Brazil</strong>
        </div>
        <div className="box">
          <h3>quick links</h3>
          <Link to={"/"}>home</Link>
          <Link to={"/userPage"}>user</Link>
          <Link to={"/shopKart"}>invoice</Link>
        </div>
        <div className="box">
          <h3>our social media</h3>
          <strong>facebook</strong>
          <strong>instagram</strong>
          <strong>twitter</strong>
        </div>
      </div>

      <h1 className="credit">
        Created by <span> Indra Bootcamp FrontEnd and BackEnd teams </span>
      </h1>
    </section>
  );
}
