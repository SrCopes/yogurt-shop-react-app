import React, { useState, useEffect, useContext } from "react";
// import Navbar from "./NavbarComponent"
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "./InvoicePage.scss";
import { useLocation } from "react-router-dom";
import { Context } from "../App";

export default function InvoicePage(props) {
  const [popup, setPopup] = useState(false);

  const [buy, setBuy] = useState(0);

  const { shoppingItems } = useContext(Context);


  //const[kart,setKart]=useState([]);

  function handleClick(e) {
    setPopup(!popup);
    setBuy(e.target.id);
    console.log(e.target.id);
  }

  return (
    <Container className="Cont">
      <br />
      {console.log(shoppingItems)}
      {shoppingItems.map((item) => {
        return (
          <div key={item.id}>
            <Row>
              <Col className="col2" xs={12}>
                <h4 onClick={handleClick} id={item.id} className="h4R">
                  {item.products}{item.total}
                </h4>
                
              </Col>
            </Row>
            {popup && buy == item.id ? (
              <div className="popUp">
                {item.products}
                {<img className="imagePop" src={item.img} alt="" />}
                <h6>{item.total}</h6>
              </div>
            ) : null}
          </div>
        );
      })}
      
    </Container>
  );
}
